sphinxcontrib-applehelp
sphinxcontrib-devhelp
sphinxcontrib-jsmath
sphinxcontrib-htmlhelp>=2.0.0
sphinxcontrib-serializinghtml>=1.1.5
sphinxcontrib-qthelp
Jinja2>=2.3
Pygments>=2.0
docutils<0.19,>=0.14
snowballstemmer>=1.1
babel>=1.3
alabaster<0.8,>=0.7
imagesize
requests>=2.5.0
packaging

[:python_version < "3.10"]
importlib-metadata>=4.4

[:sys_platform=="win32"]
colorama>=0.3.5

[docs]
sphinxcontrib-websupport

[lint]
flake8>=3.5.0
isort
mypy>=0.950
docutils-stubs
types-typed-ast
types-requests

[test]
pytest>=4.6
html5lib
cython

[test:python_version < "3.8"]
typed_ast
